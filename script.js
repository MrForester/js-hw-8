// Завдання 1

let p = document.getElementsByTagName('p');

for (let elem of p){
    elem.style.background = '#ff0000';
};

// Завдання 2

let elem1 = document.getElementById('optionsList');

    console.log(elem1);

let parentElement = elem1.parentElement; 

    console.log(parentElement);


for (let node of document.getElementById('optionsList').childNodes) {
    let nodeInfo = "Node name:"+ " " + node.nodeName + " " + "Node type:" + " " + node.nodeType;
    console.log(nodeInfo);
  };

// Завдання 3

let text = document.createElement('p');
    text.className = "testParagraph";
    text.innerHTML = "This is a paragraph";
    document.querySelector('.section-options').prepend(text);
    console.log(text);

// Завдання 4,5 


let elemNew = document.querySelector('.main-header');

for (let i = 0; i < elemNew.children.length; i++) {
    console.log( elemNew.children[i]); 
};

for (let i = 0; i < elemNew.children.length; i++) {
    let newClassElement = elemNew.children[i];
    newClassElement.classList.add('nav-item');
    console.log(newClassElement);
};

// Завдання 6 

let lastTask = document.querySelectorAll('.section-title');
    
for(let oneLastTask of lastTask){
    oneLastTask.classList.remove('section-title')
    console.log(oneLastTask);
};




